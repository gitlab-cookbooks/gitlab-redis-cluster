# Cookbook:: gitlab-redis-cluster
# Recipe:: default
# License:: MIT
#
# Copyright:: 2020, GitLab Inc.

node.default['ark']['package_dependencies'] = []
include_recipe 'ark::default'

env = node.chef_environment
node.default['redis-cluster']['secrets'] = {
  backend: 'gkms',
  path: {
    path: "gitlab-#{env}-secrets/redis-cluster",
    item: "#{env}.enc",
  },
  key: {
    ring: 'gitlab-secrets',
    key: env,
    location: 'global',
  },
} if !node['cloud'].nil? && node['cloud']['provider'] == 'gce'

user node['redis-cluster']['unix_user'] do
  manage_home false
end

redis_cluster_secrets = secrets_for(node['redis-cluster']['secrets'])

cluster_name = node['redis-cluster']['cluster_name']

base_dir = node['redis-cluster']['dir']
data_dir = node['redis-cluster']['data_dir']
log_dir = node['redis-cluster']['log_dir']
redis_log_path = "#{log_dir}/redis.log"

[base_dir, log_dir].each do |dir|
  directory dir do
    owner node['redis-cluster']['unix_user']
    group node['redis-cluster']['unix_group']
    mode '0755'
    action :create
  end
end

["#{base_dir}/etc", data_dir].each do |dir|
  directory dir do
    owner node['redis-cluster']['unix_user']
    group node['redis-cluster']['unix_group']
    mode '0750'
    action :create
  end
end

raw_redis_conf = node['redis-cluster']['redis_conf'].dup
raw_redis_conf.merge!(redis_cluster_secrets[cluster_name]['redis_conf']) if redis_cluster_secrets[cluster_name] && redis_cluster_secrets[cluster_name]['redis_conf']

if node['redis-cluster']['hostname']
  raw_redis_conf['cluster-announce-hostname'] = `hostname -f`.strip
  raw_redis_conf['cluster-preferred-endpoint-type'] = 'hostname'
end

redis_conf = raw_redis_conf.flat_map do |key, values|
  values = [values] unless values.respond_to?(:map)
  values.map { |value| "#{key} #{value}" }
end.join("\n")

file "#{base_dir}/etc/redis.conf" do
  content redis_conf + "\n"
  mode '0640'
  owner node['redis-cluster']['unix_user']
  group node['redis-cluster']['unix_group']
end

ark 'redis' do
  url node['redis-cluster']['package']['url']
  version node['redis-cluster']['package']['version']
  checksum node['redis-cluster']['package']['sha256sum']
  extension 'tar.gz'
  path ::File.dirname(base_dir)
  owner node['redis-cluster']['unix_user']
  group node['redis-cluster']['unix_group']
  action :put
end

redis_env = node['redis-cluster']['env'].map do |var, value|
  "\"#{var}=#{value}\""
end.join(' ')

systemd_unit 'redis-server.service' do
  content(
    {
      Unit: {
        Description: 'Run a redis-server process.',
        After: 'network.target',
      },
      Service: {
        Type: 'simple',
        Environment: redis_env,
        ExecStart: "#{base_dir}/redis-server #{base_dir}/etc/redis.conf",
        WorkingDirectory: data_dir,
        KillMode: 'process',
        Restart: 'always',
        RestartSec: '5s',
        User: node['redis-cluster']['unix_user'],
        LimitNOFILE: 'infinity',
      },
      Install: {
        WantedBy: 'multi-user.target',
      },
    }
  )
  action [:create, :enable, :start]
end

cookbook_file "#{base_dir}/gitlab-redis-cli" do
  source 'gitlab-redis-cli'
  owner 'root'
  group 'root'
  mode '0755'
end

link '/usr/local/bin/gitlab-redis-cli' do
  to "#{base_dir}/gitlab-redis-cli"
end

cookbook_file "#{base_dir}/redis-cluster-failover-if-primary" do
  source 'redis-cluster-failover-if-primary'
  owner 'root'
  group 'root'
  mode '0755'
end

link '/usr/local/bin/redis-cluster-failover-if-primary' do
  to "#{base_dir}/redis-cluster-failover-if-primary"
end

include_recipe 'logrotate::default'

logrotate_app :redis do
  path redis_log_path
  options %w(missingok compress delaycompress notifempty)
  rotate 6
  frequency 'hourly'
end

include_recipe 'gitlab-redis-cluster::shard-checker'
