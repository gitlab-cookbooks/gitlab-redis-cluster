# Cookbook:: gitlab-redis-cluster
# Recipe:: shard-checker
# License:: MIT
#
# Copyright 2023, GitLab Inc.
#
# Periodically check if any Redis Cluster shards have become masterless.
# Force promotion of a healthy replica if Redis native quorum-based failover authorization
# does not complete within a configurable time limit.

base_dir = node['redis-cluster']['dir']
cluster_name = node['redis-cluster']['cluster_name']

script_path = "#{base_dir}/gitlab-redis-cluster-shard-checker.sh"
conf_file_path = "#{base_dir}/etc/gitlab-redis-cluster-shard-checker.conf"

node.default['redis-cluster']['shard-checker']['conf']['CONSUL_LOCK_NAME_PREFIX'] = "redis-cluster-shard-checker/#{cluster_name}"

file conf_file_path do
  content node['redis-cluster']['shard-checker']['conf'].map { |k, v| "#{k}=#{v}\n" }.join
  owner node['redis-cluster']['unix_user']
  group node['redis-cluster']['unix_group']
  mode '0644'
end

cookbook_file script_path do
  source 'gitlab-redis-cluster-shard-checker.sh'
  owner node['redis-cluster']['unix_user']
  group node['redis-cluster']['unix_group']
  mode '0755'
end

shard_checker_service_unit = {
  Unit: {
    Description: 'Timer-run service to periodically check Redis Cluster for masterless shards',
  },
  Service: {
    Type: 'oneshot',
    ExecStart: "#{script_path} #{conf_file_path}",
    TimeoutStartSec: node['redis-cluster']['shard-checker']['max_check_duration_seconds'],
    User: 'root',
  },
}

shard_checker_timer_unit = {
  Unit: {
    Description: 'Timer to periodically check Redis Cluster for masterless shards',
  },
  Timer: {
    OnCalendar: node['redis-cluster']['shard-checker']['systemd_timer']['on_calendar'],
    AccuracySec: '1s',
  },
  Install: {
    WantedBy: 'timers.target',
  },
}

systemd_unit 'redis-cluster-shard-checker.service' do
  content shard_checker_service_unit
  action [:create, :enable]
end

running_action = node['redis-cluster']['shard-checker']['systemd_timer']['enabled'] ? :start : :stop
enabled_action = node['redis-cluster']['shard-checker']['systemd_timer']['enabled'] ? :enable : :disable

systemd_unit 'redis-cluster-shard-checker.timer' do
  content shard_checker_timer_unit
  action [:create, enabled_action, running_action]
end
