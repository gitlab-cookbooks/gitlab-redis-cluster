#!/bin/bash

# Purpose:
# This aims to handle the corner case where Redis has a masterless shard but cannot reach quorum to promote a replica.
# In such a situation, this script waits a while and then manually forces promotion without quorum if possible.
#
# Assumptions:
# * All nodes in the cluster periodically run this script via systemd units (a oneshot service and a timer). This ensures
#   any node can act on behalf of any shard.
# * This script expects to be run by "consul lock" while holding a mutex, to prevent multiple instances of this script
#   from concurrently taking action on the same redis cluster. For testing without consul, you may set CONSUL_LOCK_HELD=true,
#   but for practical use, let it use consul.

set -o pipefail

CONFIG_FILE=${1:-/opt/redis/etc/gitlab-redis-cluster-shard-checker.conf}

function main()
{
    load_config
    acquire_consul_lock_for_cluster_shard_checker "$@"
    check_cluster
    publish_prometheus_metrics
}

function acquire_consul_lock_for_cluster_shard_checker()
{
    # When "consul lock" starts a child process (such as this script), it sets the CONSUL_LOCK_HELD environment variable.
    # If that variable is not set to "true", we re-exec this process as a "consul lock" call, which will acquire the lock
    # and then create a child process to run this script again, this time with CONSUL_LOCK_HELD=true.

    [[ "$CONSUL_LOCK_HELD" == "true" ]] && return
    log "Acquiring lock from consul (occasional timeouts are ok): $CONSUL_LOCK_NAME_PREFIX"
    exec "$CONSUL_CLI" lock -verbose=true -timeout=1ms -shell=false "$CONSUL_LOCK_NAME_PREFIX" "$0" "$@"
}

function load_config()
{
    [[ -r "$CONFIG_FILE" ]] || die "Config file is missing or not readable: $CONFIG_FILE"
    . "$CONFIG_FILE"

    is_number "$MIN_SECONDS_DELAY_BEFORE_FORCING_PROMOTION" || die "Config error: MIN_SECONDS_DELAY_BEFORE_FORCING_PROMOTION must be a positive integer."
    is_number "$REDIS_PORT" || die "Config error: REDIS_PORT must be a number."
    [[ -x "$REDIS_CLI" ]] || die "Config error: REDIS_CLI is missing or not executable: '$REDIS_CLI'"
    [[ -x "$CONSUL_CLI" ]] || die "Config error: CONSUL_CLI is missing or not executable: '$CONSUL_CLI'"
    [[ -n "$CONSUL_LOCK_NAME_PREFIX" ]] || die "Config error: CONSUL_LOCK_NAME_PREFIX should be set to a cluster-specific string."
}

function check_cluster()
{
    is_cluster_mode_enabled_on_local_redis || die "Local redis is not using cluster mode."
    log "Checking cluster state. If unhealthy, will wait up to $MIN_SECONDS_DELAY_BEFORE_FORCING_PROMOTION seconds for auto-recovery."
    local AUTORECOVERY_DEADLINE=$( date +%s -d "$MIN_SECONDS_DELAY_BEFORE_FORCING_PROMOTION seconds" )
    while [[ $( date +%s ) -le "$AUTORECOVERY_DEADLINE" ]] ; do
        is_local_redis_node_up || die "Cannot connect and authenticate to local redis instance. Is it down?"
        if is_cluster_state_ok ; then
            log "Cluster state is healthy. Nothing to do."
            return
        fi
        sleep 1
    done
    log "Cluster state has remained unhealthy for more than $MIN_SECONDS_DELAY_BEFORE_FORCING_PROMOTION seconds."
    show_local_node_view_of_cluster_state "Found cluster remained unhealthy for over $MIN_SECONDS_DELAY_BEFORE_FORCING_PROMOTION seconds"

    log "Finding failed master nodes."
    FAILED_MASTER_NODE_IDS=( $( find_node_ids_of_failed_masters_still_assigned_slots ) )
    for FAILED_MASTER_NODE_ID in "${FAILED_MASTER_NODE_IDS[@]}"
    do
        # Is this node still a failed master? If there were multiple failed masters, ensure each previously detected failure still exists before acting on it.
        is_master_node_failed "$FAILED_MASTER_NODE_ID" || continue
        log "Found failed master node: $FAILED_MASTER_NODE_ID"
        promote_replica_of_failed_master "$FAILED_MASTER_NODE_ID"
    done

    log "Finished recovery attempts"
    if is_cluster_state_ok ; then
        log "Cluster state is healthy from local node's perspective."
    else
        log "Cluster state is not healthy from local node's perspective."
    fi
}

function is_cluster_mode_enabled_on_local_redis()
{
    run_redis_command --raw CONFIG GET cluster-enabled 2>&1 | grep -q 'yes'
}

function is_local_redis_node_up()
{
    run_redis_command --raw PING 2>&1 | grep -q 'PONG'
}

function is_cluster_state_ok()
{
    run_redis_command --raw CLUSTER INFO 2>&1 | grep -q 'cluster_state:ok'
}

function show_local_node_view_of_cluster_state()
{
    local COMMENT=$1
    log "Showing CLUSTER INFO as of: $COMMENT"
    run_redis_command --raw CLUSTER INFO
    log "Showing CLUSTER NODES as of: $COMMENT"
    run_redis_command --raw CLUSTER NODES
}

function find_node_ids_of_failed_masters_still_assigned_slots()
{
    # Find failed masters that have not yet been replaced by an earlier promotion:
    #  * Field 3 (flags) must include "master" and either "fail" or "fail?".
    #  * Field 9 (first of potentially many hash slot ranges) must be non-blank, indicating slots are still assigned to this failed master.
    # For reference, see the CLUSTER NODES output format spec: https://redis.io/commands/cluster-nodes/
    run_redis_command --raw CLUSTER NODES | awk '$3 ~ /fail/ && $3 ~ /master/ && $9 ~ /[0-9]+/ { print $1 }'
}

function is_master_node_failed()
{
    local MASTER_NODE_ID=$1
    find_node_ids_of_failed_masters_still_assigned_slots | grep -q "$MASTER_NODE_ID"
}

function choose_healthy_replica_to_promote()
{
    local FAILED_MASTER_NODE_ID="$1"
    run_redis_command --raw CLUSTER REPLICAS "$FAILED_MASTER_NODE_ID" | awk '$3 !~ /fail/ { print $1 }' | sort | head -n1
}

function promote_replica_of_failed_master()
{
    local FAILED_MASTER_NODE_ID=$1
    local REPLICA_NODE_ID_TO_PROMOTE=$( choose_healthy_replica_to_promote "$FAILED_MASTER_NODE_ID" )
    if [[ -z "$REPLICA_NODE_ID_TO_PROMOTE" ]] ; then
        log "Could not find a healthy replica to promote for failed master!"
        return
    fi
    log "Promoting replica via TAKEOVER: Replica $REPLICA_NODE_ID_TO_PROMOTE replacing its failed master."
    run_remote_redis_command_for_node_id "$REPLICA_NODE_ID_TO_PROMOTE" CLUSTER FAILOVER TAKEOVER
    TAKEOVER_COUNT=$(( $TAKEOVER_COUNT + 1 ))
    log "Waiting after promotion for cluster state to settle."
    wait_for_promotion_to_finish_and_cluster_config_epoch_to_propagate
    show_local_node_view_of_cluster_state "Promoted replica $REPLICA_NODE_ID_TO_PROMOTE to replace failed master $FAILED_MASTER_NODE_ID"
}

function wait_for_promotion_to_finish_and_cluster_config_epoch_to_propagate()
{
    # The CLUSTER FAILOVER command is synchronous (rather than scheduled) when using the TAKEOVER option.
    # However, because the node increments the cluster config-epoch without authorization or coordination among other masters,
    # we need to wait a few seconds for that new cluster config-epoch to propagate via gossip to the remaining nodes.
    # Otherwise, if we have another failed shard, the next promotion could collide, using the same cluster config-epoch number,
    # causing the cluster to discard one of those config changes.
    # See the redis docs: https://redis.io/commands/cluster-failover/#implementation-details-and-notes
    #
    # Additionally, we want to pause between each successful promotion to give redis another chance to attempt quorum-based promotions.
    # When a majority of masters are up, that mechanism can take over, so this script may not need to force promotion for all of the
    # failed primaries.
    #
    # Currently for simplicity we just optimistically wait a few seconds.
    # Alternately we could poll CLUSTER INFO to watch for both of the following:
    #  * The promoted node acquires the "master" flag (field 3).
    #  * All connected nodes share the same "config-epoch" (field 7), matching the promoted node's epoch.
    # But we may still need to sleep after that, in case redis has regained quorum to finish handling the remaining failed masters.
    sleep "$MIN_SECONDS_DELAY_BEFORE_FORCING_PROMOTION"
}

function publish_prometheus_metrics()
{
    if [[ -z "$PROMETHEUS_METRICS_OUTPUT_FILE" || ! -d $( dirname "$PROMETHEUS_METRICS_OUTPUT_FILE" ) ]] ; then
        log "Skipping update of prometheus metrics."
        return
    else
        log "Updating prometheus metrics."
        cat <<HERE > "$PROMETHEUS_METRICS_OUTPUT_FILE"
# TYPE redis_cluster_shard_checker_last_run_timestamp_seconds gauge
# HELP redis_cluster_shard_checker_last_run_timestamp_seconds The unix timestamp of the end of the last run of the redis cluster shard checker.
redis_cluster_shard_checker_last_run_timestamp_seconds $( date +%s )

# TYPE redis_cluster_shard_checker_failed_master_count gauge
# HELP redis_cluster_shard_checker_failed_master_count The number of failed master nodes detected at the start of this check.
redis_cluster_shard_checker_failed_master_count ${#FAILED_MASTER_NODE_IDS[@]}

# TYPE redis_cluster_shard_checker_forced_failover_count gauge
# HELP redis_cluster_shard_checker_forced_failover_count The number of replicas promoted via CLUSTER FAILOVER TAKEOVER. May be less than failed masters count if Redis helped after reaching quorum.
redis_cluster_shard_checker_forced_failover_count ${TAKEOVER_COUNT:-0}
HERE
    fi
}

function is_number()
{
    local value=$1
    local regex='^[0-9][0-9]*$'
    [[ $value =~ $regex ]]
}

function run_redis_command()
{
    $REDIS_CLI -h localhost -p "$REDIS_PORT" "$@"
    [[ $? -eq 0 ]] || die "Failed to run redis-cli command with args: $@"
}

function run_remote_redis_command_for_node_id()
{
    local REDIS_NODE_ID=$1
    shift

    local REMOTE_IP_AND_PORT=$( run_redis_command --raw CLUSTER NODES | grep "^$REDIS_NODE_ID" | awk '{ print $2 }' | cut -d'@' -f1 )
    [[ $REMOTE_IP_AND_PORT =~ ^[0-9][0-9.]*:[0-9][0-9]*$ ]] || die "Could not parse IP and port for redis node id $REDIS_NODE_ID"
    local REMOTE_IP=$( echo "$REMOTE_IP_AND_PORT" | cut -d':' -f1 )
    local REMOTE_PORT=$( echo "$REMOTE_IP_AND_PORT" | cut -d':' -f2 )

    $REDIS_CLI -h "$REMOTE_IP" -p "$REMOTE_PORT" "$@"
    [[ $? -eq 0 ]] || die "Failed to run redis-cli command with args: $@"
}

function die()
{
    local message=$1
    echo "$( date +%Y-%m-%d\ %H:%M:%S\ %Z )  ERROR $message" 1>&2
    exit 1
}

function log()
{
    local message=$1
    echo "$( date +%Y-%m-%d\ %H:%M:%S\ %Z )  LOG $message"
}

main "$@"
