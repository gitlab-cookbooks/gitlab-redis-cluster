default['redis-cluster']['dir'] = '/opt/redis'
default['redis-cluster']['data_dir'] = '/opt/redis/data'
default['redis-cluster']['log_dir'] = '/var/log/redis'
default['redis-cluster']['unix_user'] = 'gitlab-redis'
default['redis-cluster']['unix_group'] = 'gitlab-redis'

default['redis-cluster']['package'] = {}
default['redis-cluster']['package']['url'] = 'https://gitlab.com/gitlab-com/gl-infra/redis-build/-/package_files/60778301/download'
default['redis-cluster']['package']['sha256sum'] = '509d7494fd264381c41d44818a8a048fb7298be3f678162463908845ee19a5c0'
default['redis-cluster']['package']['version'] = '7.0.5'

default['redis-cluster']['redis_conf'] = {
    'cluster-allow-replica-migration': 'no',  # Disabled to allow statically assigning nodes in different zones to each shard.
    'cluster-config-file': 'nodes.conf',
    'cluster-enabled': 'yes',
    'cluster-node-timeout': 5000,             # Timeout (milliseconds) for cluster node health checks (PING).
    'logfile': '/var/log/redis/redis.log',
    'maxclients': 50000,                      # Upper bound is redis-server process max file descriptors (LimitNOFILE=infinity).
    'rename-command': 'KEYS ""',              # Prevent accidentally running the very expensive "KEYS" command.
    'repl-diskless-sync': 'yes',
    'tcp-keepalive': 60,                      # TCP keepalive interval (seconds)
    'timeout': 1200,                          # TCP idle timeout (seconds)
}

default['redis-cluster']['cluster_name'] = 'default'
default['redis-cluster']['env'] = {}
default['redis-cluster']['hostname'] = true

# Values for automated testing; in practice, you want GKMS
default['redis-cluster']['secrets']['backend'] = 'chef_vault'
default['redis-cluster']['secrets']['path'] = 'secrets'
default['redis-cluster']['secrets']['key'] = 'redis-cluster'

# Redis Cluster Shard Checker config:
default['redis-cluster']['shard-checker']['conf']['REDIS_CLI'] = '/opt/redis/gitlab-redis-cli'
default['redis-cluster']['shard-checker']['conf']['REDIS_PORT'] = 6379
default['redis-cluster']['shard-checker']['conf']['MIN_SECONDS_DELAY_BEFORE_FORCING_PROMOTION'] = 15
default['redis-cluster']['shard-checker']['conf']['PROMETHEUS_METRICS_OUTPUT_FILE'] = '/opt/prometheus/node_exporter/metrics/redis-cluster-shard-checker.prom'
default['redis-cluster']['shard-checker']['conf']['CONSUL_CLI'] = '/usr/local/bin/consul'

# Max duration for each run of the oneshot systemd service unit:
default['redis-cluster']['shard-checker']['max_check_duration_seconds'] = 60

# Cron schedule for running the shard checker:
# Each node in the cluster will try to run the checker on this schedule, but only one at a time will
# acquire the lock to actually perform the check.
default['redis-cluster']['shard-checker']['systemd_timer']['enabled'] = true
default['redis-cluster']['shard-checker']['systemd_timer']['on_calendar'] = '*-*-* *:*:25'
