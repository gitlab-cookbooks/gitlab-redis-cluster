# InSpec tests for recipe gitlab-redis-cluster::default

control 'basic-install-of-redis-cluster' do
  impact 1.0
  title 'General tests for gitlab-redis-cluster cookbook'
  desc '
    This control ensures that:
      * all the basic files are created, with expected contents
      * services are enabled'

  describe systemd_service('redis-server.service') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
