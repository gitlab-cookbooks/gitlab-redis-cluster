name             'gitlab-redis-cluster'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Deploys a Redis Cluster shard'
version          '0.2.5'
chef_version     '>= 14.0'
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab-redis-cluster/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab-redis-cluster'

supports 'ubuntu', '= 20.04'

# Please specify dependencies with version pin:
# depends 'cookbookname', '~> 1.0.0'
depends 'gitlab_secrets'
depends 'ark'
depends 'logrotate', '~> 2.2.0'
