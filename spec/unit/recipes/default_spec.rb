# Cookbook:: gitlab-redis-cluster
# Spec::cpu_profile
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab-redis-cluster::default' do
  context 'when all attributes are default, on Ubuntu 20.04' do
    platform 'ubuntu', '20.04'

    normal_attributes['redis-cluster']['secrets'] = {
      'backend' => 'gkms',
      'path' => 'redis-cluster',
      'key' => 'key',
    }
    normal_attributes['gce']['project']['projectId'] = 'spec_project'

    before do
      expect_secrets('redis-cluster')
      allow_any_instance_of(Chef::Node).to receive(:environment)
        .and_return('spec')
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates a user' do
      expect(chef_run).to create_user('gitlab-redis')
    end

    it 'creates a systemd service unit' do
      expect(chef_run).to create_systemd_unit('redis-server.service')
      expect(chef_run).to enable_systemd_unit('redis-server.service')
    end

    it 'deploys gitlab-redis-cli' do
      expect(chef_run).to create_cookbook_file('/opt/redis/gitlab-redis-cli')
      expect(chef_run).to create_link('/usr/local/bin/gitlab-redis-cli')
    end

    it 'deploys redis-cluster-failover-if-primary' do
      expect(chef_run).to create_cookbook_file('/opt/redis/redis-cluster-failover-if-primary')
      expect(chef_run).to create_link('/usr/local/bin/redis-cluster-failover-if-primary')
    end

    it 'rotates log' do
      expect(chef_run).to enable_logrotate_app('redis').with(
        path: '/var/log/redis/redis.log',
        options: %w(missingok compress delaycompress notifempty),
        rotate: 6,
        frequency: 'hourly'
      )
    end
  end
end
