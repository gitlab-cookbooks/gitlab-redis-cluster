# Cookbook:: gitlab-redis-cluster
# Spec::shard_checker
#
# Copyright:: 2023, MIT

require 'spec_helper'

describe 'gitlab-redis-cluster::shard-checker' do
  context 'when all attributes are default, on Ubuntu 20.04' do
    platform 'ubuntu', '20.04'

    normal_attributes['gce']['project']['projectId'] = 'spec_project'

    before do
      allow_any_instance_of(Chef::Node).to receive(:environment)
        .and_return('spec')
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates a systemd service unit' do
      expect(chef_run).to create_systemd_unit('redis-cluster-shard-checker.service')
      expect(chef_run).to enable_systemd_unit('redis-cluster-shard-checker.timer')
    end

    it 'deploys gitlab-redis-cli' do
      expect(chef_run).to create_cookbook_file('/opt/redis/gitlab-redis-cluster-shard-checker.sh')
      expect(chef_run).to create_file('/opt/redis/etc/gitlab-redis-cluster-shard-checker.conf')
    end
  end
end
